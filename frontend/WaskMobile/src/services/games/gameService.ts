import {GenericService} from '../genericService';
// To define type
export class GameService extends GenericService<any> {
  constructor() {
    super('games');
  }

  getLastGame() {
    return this.collection.orderBy('timestamp', 'desc').limit(1).get();
  }
}
