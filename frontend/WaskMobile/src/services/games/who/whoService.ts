import {UserService} from '../../../services/userService';
import {GameService} from '../gameService';
import {WhoAnswerService} from './whoAnswerService';
import {WhoQuestionService} from './whoQuestionService';
import { PictureService } from '../pictureService';

export class WhoService {
  private gameService = new GameService();
  private questionService = new WhoQuestionService();
  private answerService = new WhoAnswerService();
  private pictureService = new PictureService();

  private userService = new UserService();
  constructor() {}

  public async startGame(): Promise<void> {
    await this.gameService.create({
      type: 'who',
      questionId: 'yNkMvHp9gZQcASnb1zyW',
      timestamp: new Date().toISOString(),
    });
  }

  public async getQuestion(gameId: string): Promise<string> {
    const game = await this.gameService.get(gameId);
    if (!game) return 'No game found';
    const question = await this.questionService.get(game.questionId);
    if (!question) return 'No question found';
    return question.text;
  }

  
  public async postAnswer(userId: string, answer: string,comment:string): Promise<void> {
    await this.answerService.create({
      userId,
      answer,
      comment,
      gameId: 'PE06ABDbiOshd1Af4HrD',
      timestamp: new Date().toISOString(),
      type:"who"
    });
  }

public async getAllAnswers(friendsIds: string[]): Promise<{ answers: any[], pictures: any[] }> { 
  const answers = await this.answerService.getAnswersOfFriends(friendsIds);
  const pictures = await this.pictureService.getPicturesOfFriends(friendsIds);

  return { answers, pictures };
}
public async getAnswers(friendsIds: string[]): Promise<any[]> {    return this.answerService.getAnswersOfFriends(friendsIds);  }
  

  public async choiceFriend(isGood: boolean, userId: string): Promise<void> {
    const user = await this.userService.get(userId);
    if (!user) return;

    user.score += isGood ? 1 : -0;
    await this.userService.update(userId, user);
    console.log(user.score);
  }

  // Choisir un ami à associer à la réponse
  public async ValidateFriend(isGood: boolean, userId: string, answerId: string): Promise<void> {
    const user = await this.userService.get(userId);
    if (!user) return;

    // On récupère la réponse associée à la question pour la valider
    const answer = await this.answerService.get(answerId);
    if (!answer) return;

    // Si le choix est correct, on ajoute l'ID de l'utilisateur au tableau des utilisateurs corrects
    if (isGood) {
      if (!answer.correctUserIds) {
        answer.correctUserIds = [];
      }

      // Eviter les doublons dans le tableau
      if (!answer.correctUserIds.includes(userId)) {
        answer.correctUserIds.push(userId);
      }

      // Mettre à jour le score de l'utilisateur
      user.score += 1;
    } else {
      // Si ce n'est pas correct, on décrémente le score
      user.score -= 0;
    }

    // Mettre à jour la réponse et l'utilisateur
    await this.answerService.update(answerId, answer);
    await this.userService.update(userId, user);
    console.log(user.score);
  }
}
