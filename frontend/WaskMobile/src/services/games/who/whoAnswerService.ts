import {WhoAnswerModel} from 'src/models/Who';
import {GenericService} from '../../genericService';

export class WhoAnswerService extends GenericService<WhoAnswerModel> {
  constructor() {
    super('whoAnswers');
  }

  async getAnswersOfFriends(friendsIds: string[]): Promise<any[]> {
    const BATCH_SIZE = 10; // Taille maximum du tableau pour Firestore "in" (max 10)
    const answers: any[] = [];

    const batches = [];
    for (let i = 0; i < friendsIds.length; i += BATCH_SIZE) {
      const batch = friendsIds.slice(i, i + BATCH_SIZE);
      batches.push(batch);
    }
    const promises = batches.map(batch =>
      this.collection
        .where('userId', 'in', batch)
        .get()
        .then(snapshot => {
          snapshot.forEach(doc => {
            // Ajoute l'ID du document à chaque réponse récupérée
            answers.push({
              ...doc.data(),  // Les données du document
              id: doc.id,     // Ajoute l'ID Firestore
            });
          });
        })
        .catch(error => {
          console.error('Error fetching answers: ', error);
          throw error;
        }),
    );

    await Promise.all(promises);
    return answers;
  }
}
