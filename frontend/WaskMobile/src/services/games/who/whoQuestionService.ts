import { WhoQuestionModel } from 'src/models/Who';
import { GenericService } from '../../genericService';

export class WhoQuestionService extends GenericService<WhoQuestionModel> {
  constructor() {
    super('whoQuestions');
  }

  // Retourne la dernière question (un seul objet)
  getLastQuestion() {
    return this.collection.orderBy('timestamp', 'desc').limit(1).get();
  }
}
