import { GenericService } from '../genericService';
import { firebase } from '@react-native-firebase/auth';

interface Picture {
  comment: string;
  userId: string;
  image: {
    name: string;
    type: string;
    uri: string;
  };
}

export class PictureService extends GenericService<Picture> {
  constructor() {
    super('pictures');
  }

  async getPictures(): Promise<Picture[]> {
    try {
      const snapshot = await this.collection.get();
      const pictures: Picture[] = snapshot.docs.map(doc => {
        const data = doc.data();
        return {
          comment: data.comment,
          userId: data.userId,  // Ajout du userId
          timestamp:data.timestamp,
          image: {
            name: data.image.name,
            type: data.image.type,
            uri: data.image.uri
          }
        };
      });

      return pictures;
    } catch (error) {
      console.error('Error fetching pictures:', error);
      return [];
    }
  }

  async createPicture(comment: string, id: string, data: any): Promise<void> {
    try {
      await this.collection.add({
        comment: comment,
        userId: id,
        ...data,
        createdAt: firebase.firestore.FieldValue.serverTimestamp(),
        timestamp: new Date().toISOString(),
      });
      console.log('Picture successfully added!');
    } catch (error) {
      console.error('Error adding picture:', error);
    }
  }

  public async getPicturesOfFriends(friendsIds: string[]): Promise<any[]> {
    try {
      // Vérifier si la liste d'amis est vide
      if (!friendsIds || friendsIds.length === 0) {
        return [];
      }
  
      // Récupérer les photos de tous les amis
      const snapshot = await firebase.firestore()
        .collection('pictures') // Assure-toi que c'est le bon nom de collection
        .orderBy('createdAt', 'desc') // Trier par date d'ajout
        .get();
  
      // Transformer les documents Firestore en objets utilisables
      const pictures = snapshot.docs.map(doc => ({
        id: doc.id,
        ...doc.data()
      }));
  
      return pictures;
    } catch (error) {
      console.error('Erreur lors de la récupération des images des amis :', error);
      return [];
    }
  }
  

}
