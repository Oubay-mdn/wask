import firestore, {
  FirebaseFirestoreTypes,
} from '@react-native-firebase/firestore';

export class GenericService<T> {
  protected readonly collection: FirebaseFirestoreTypes.CollectionReference;

  constructor(collectionName: string) {
    this.collection = firestore().collection(collectionName);
  }
  public async create(
    data: FirebaseFirestoreTypes.DocumentData,
  ): Promise<void> {
    await this.collection.add(data);
  }


  public async get(
    id: string,
  ): Promise<FirebaseFirestoreTypes.DocumentData | undefined> {
    const documentSnapshot = await this.collection.doc(id).get();
    return documentSnapshot.data();
  }

  public async update(
    id: string,
    updatedData: Partial<FirebaseFirestoreTypes.SetValue<T>>,
  ): Promise<void> {
    await this.collection.doc(id).update(updatedData);
  }

  public async delete(id: string): Promise<void> {
    await this.collection.doc(id).delete();
  }

  public getCollection(): FirebaseFirestoreTypes.CollectionReference {
  return this.collection;
}
}
