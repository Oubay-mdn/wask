import {UserModel} from 'src/models/User';
import {GenericService} from './genericService';

export class UserService extends GenericService<UserModel> {
  constructor() {
    super('users');
  }

  public async getUserFriends(
    userId: string,
    fields?: string[],
  ): Promise<any[]> {
    const documentSnapshot = await this.collection.doc(userId).get();
    const userData = documentSnapshot.data();

    if (!userData) {
      throw new Error('User data not found');
    }

    const friendsIds = userData.friends || [];
    const friends = await Promise.all(
      friendsIds.map((id: string) => this.getUserExpectedField(id, fields)),
    );

    return friends;
  }
  public async getUserExpectedField(
    userId: string,
    fields?: string[],
  ): Promise<any> {
    const documentSnapshot = await this.collection.doc(userId).get();
    const userData = documentSnapshot.data();

    if (!fields) {
      return userData;
    }

    const selectedUserData: any = {};
    fields.forEach(field => {
      if (userData && field in userData) {
        selectedUserData[field] = userData[field];
      }
    });

    return selectedUserData;
  }

  public async searchUser(username: string): Promise<any | undefined> {
    const querySnapshot = await this.collection
      .where('username', '==', username)
      .get();
    if (querySnapshot.empty) {
      return undefined;
    }
    const userData = querySnapshot.docs[0].data();
    return userData;
  }

  public async addFriend(userId: string, friendId: string): Promise<void> {
    try {
      console.log('start function add friend');
      const user: any = await this.getUserFriends(userId, ['friends']);

      if (!user) {
        throw new Error(`User with id ${userId} does not exist`);
      }

      let friends = user.friends || [];
      if (friends.includes(friendId)) {
        console.log(
          `User with id ${userId} is already friends with user ${friendId}`,
        );
        throw new Error('Already friends');
      } else {
        friends.push(friendId);
      }

      const addBack = await this.get(friendId);

      if (!addBack) {
        throw new Error(`User with id ${friendId} does not exist`);
      }

      let friendsBack = addBack.friends || [];
      if (friendsBack.includes(userId)) {
        console.log(
          `User with id ${friendId} is already friends with user ${userId}`,
        );
        throw new Error('Already friends');
      } else {
        friendsBack.push(userId);
      }

      // Fetch the current user's friends
      const currentUser = await this.get(userId);

      const currentUserFriends = currentUser?.friends || [];

      // Add the new friend to the list
      currentUserFriends.push(friendId);

      await this.update(friendId, {friends: friendsBack});
      await this.update(userId, {friends: currentUserFriends}); // Update with the new list
    } catch (error) {
      // Handle the error here
      console.error('Error adding friend:', error);
      throw error; // Rethrow the error to be caught by the caller
    }
  }
}
