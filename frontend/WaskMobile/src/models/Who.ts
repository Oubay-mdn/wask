export interface WhoGameModel {
  questionId: string;
}

export interface WhoQuestionModel {
  text: string;
  category: string;
  timestamp: string;
}

export interface WhoAnswerModel {
  answerId:string;
  gameId: string;
  userId: string;
  answer: string;
  timestamp: string;
  correctUserIds:string[];
  
}
