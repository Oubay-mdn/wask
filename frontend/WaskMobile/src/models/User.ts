export interface UserModel {
  id: number;
  username: string;
  email: string;
  score: number;
  firstname: string;
  lastname: string;
  friends: string[];
}
