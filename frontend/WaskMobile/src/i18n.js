import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import translationFr from './lang/fr/translation.json';
import translationEn from './lang/en/translation.json';

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  
  .init({
    compatibilityJSON: 'v3',
    resources: {
            en: {
              translation : translationEn
            },
            fr: {
              translation: translationFr
            }
    },
    lng: "fr", 
    fallbackLng: "fr",
    interpolation: {
      escapeValue: false // react already safes from xss
    }
  });

  export default i18n;