import React from 'react';
import {Text, Image, View, StyleSheet} from 'react-native';

const HeaderTitleFeed = (props: {text: string}) => {
  const styles = StyleSheet.create({
    HeaderTitleFeedtext: {
      fontFamily: 'Quicksand',
      fontWeight: '800',
      fontSize: 20,
      letterSpacing: 0.5,
      lineHeight: 22,
      color: 'white',
    },
    headerView: {flexDirection: 'row', alignItems: 'baseline'},
    headerImage: {width: 200, height: 50},
  });

  return (
    <View style={styles.headerView}>
      <Image
        style={styles.headerImage}
        source={require('../../assets/images/Wask.png')}
        resizeMode="contain"
      />
      <Text style={styles.HeaderTitleFeedtext}>{props.text}</Text>
    </View>
  );
};
export default HeaderTitleFeed;
