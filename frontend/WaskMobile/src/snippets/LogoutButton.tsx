// LogoutButton component
import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import auth from '@react-native-firebase/auth';
import {useTranslation} from 'react-i18next';
import {useUser} from './../../config/UserContext'; // Adjust the path as needed

const LogoutButton: React.FC = () => {
  
  const {t, i18n} = useTranslation();
  const styles = StyleSheet.create({
    btn: {
      width: '90%',
      margin: 15,
      padding: 10,
      borderRadius: 10,
      backgroundColor: 'black',
      alignItems: 'center',
    },
    text: {
      fontSize: 16,
      color: '#FFFFFF',
    },
  });

  const {updateUser} = useUser();
  const {user} = useUser();
  const handleLogout = async () => {
    try {
      console.log(user);
      await auth().signOut();
      updateUser(null); // Set the user to null in the context
    } catch (error) {
      console.error('Error signing out:', error);
    }
  };

  return (
    <TouchableOpacity style={styles.btn} onPress={handleLogout}>
      <Text style={styles.text}>{t('singout')}</Text>
    </TouchableOpacity>
  );
};

export default LogoutButton;
