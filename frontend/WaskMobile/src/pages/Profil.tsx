import React, {useContext, useEffect, useState} from 'react';
import {firebase} from '@react-native-firebase/auth';
import {UserProvider, useUser} from './../../config/UserContext'; // Import the UserProvider and useUser hook
import {useTranslation} from 'react-i18next';
import {Link, useNavigation} from '@react-navigation/native';
import LogoutButton from '../snippets/LogoutButton';
import {FirebaseFirestoreTypes} from '@react-native-firebase/firestore';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ImageSourcePropType,
} from 'react-native';

const Profil: React.FC = () => {
  // const [loading, setLoading] = useState<boolean>(true);
  const {user} = useUser();
  const navigation = useNavigation<any>();
  const {t, i18n} = useTranslation();
  const changeLanguage = () => {
    i18n.resolvedLanguage === 'fr'
      ? i18n.changeLanguage('en')
      : i18n.changeLanguage('fr');
  };
  const [src] = useState<ImageSourcePropType>({
    uri: 'https://w7.pngwing.com/pngs/184/113/png-transparent-user-profile-computer-icons-profile-heroes-black-silhouette-thumbnail.png',
  });

  const [userProfile, setUserProfile] = useState<
    FirebaseFirestoreTypes.DocumentData | undefined
  >(undefined);

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      color: '#3D3D3D',
    },
    textmin: {
      fontSize: 16,
      color: '#3D3D3D',
    },
    text: {
      fontSize: 16,
      color: '#FFFFFF',
    },
    box: {
      backgroundColor: '#e0e0e0',
      marginTop: 15,
      paddingHorizontal: 20,
      paddingTop: 5,
      paddingBottom: 10,
      borderRadius: 10,
      width: '90%',
    },
    profileImg: {
      height: 80,
      width: 80,
      borderRadius: 40,
    },
    btn: {
      width: '90%',
      margin: 15,
      padding: 10,
      borderRadius: 10,
      backgroundColor: '#99009C',
      alignItems: 'center',
    },
    btnText: {
      color: '#FFFFFF'
    },
    label: {
      position: 'absolute',
      top: '50%',
      transform: [{translateY: -10}, {translateX: 15}],
      color: i18n.resolvedLanguage == 'fr' ? 'white' : '#063AC2',
    },
    label2: {
      position: 'absolute',
      top: '50%',
      transform: [{translateY: -10}, {translateX: 65}],
      color: i18n.resolvedLanguage == 'fr' ? '#063AC2' : 'white',
    },
    switcher: {
      width: 100,
      height: 50,
      flexDirection: 'row',
    },
    switchInput: {
      width: 50,
      height: 50,
      borderTopLeftRadius: 25,
      borderBottomLeftRadius: 25,
      backgroundColor: i18n.resolvedLanguage == 'fr' ? '#063AC2' : 'white',
    },
    switchInput2: {
      width: 50,
      height: 50,
      borderTopRightRadius: 25,
      borderBottomRightRadius: 25,
      backgroundColor: i18n.resolvedLanguage == 'fr' ? 'white' : '#063AC2',
    },
    center: {alignItems: 'center'},
  });

  useEffect(() => {
    const fetchUserInfo = async () => {
      if (!user) {
        return;
      }
      const {uid} = user;
      // Discard fetch when user ID not defined
      if (!uid) {
        return;
      }
      firebase
        .firestore()
        .collection('users')
        .doc(uid)
        .get()
        .then(doc => {
          const data = doc.data();
          if (data) {
            setUserProfile(data);
          } else {
            // handle the case where the data is undefined
            console.error('Cannot find user !');
          }
        });
    };

    fetchUserInfo();
  }, [user]);

  if (!user) {
    return (
      <View>
        <TouchableOpacity onPress={() => navigation.navigate(t('navigation.login'))} style={styles.btn}>
           <Text style={styles.btnText}>{t('login')}</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate(t('navigation.register'))} style={styles.btn}>
          <Text style={styles.btnText}>{t('register')}</Text>
        </TouchableOpacity>
      </View>
    );
  } else {
    return (
      <UserProvider>
        <View style={styles.container}>
          <View style={styles.center}>
            <Image source={src} style={styles.profileImg} />
            <Text style={styles.text}>{userProfile?.username}</Text>
            <Text style={styles.text}>
              {userProfile?.firstname} {userProfile?.name}
            </Text>
          </View>
          <View style={styles.switcher}>
            <TouchableOpacity
              style={styles.switchInput}
              onPress={e => changeLanguage()}
            />
            <TouchableOpacity
              style={styles.switchInput2}
              onPress={e => changeLanguage()}
            />
            <Text style={styles.label}>{'FR'}</Text>
            <Text style={styles.label2}>{'EN'}</Text>
          </View>
          <View style={styles.box}>
            <Text style={styles.textmin}>{user.email}</Text>
            <Text style={styles.textmin}>{t('bio')}</Text>
            <Text style={styles.textmin}>{userProfile?.bio}</Text>
          </View>
          <LogoutButton />
        </View>
      </UserProvider>
    );
  }
};

export default Profil;
