import { useState } from 'react';
import {
  View,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useTranslation } from 'react-i18next';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import Input from '../components/Input';

function Register() {
  const [email, setEmail] = useState<string>('');
  const [name, setName] = useState<string>('');
  const [firstname, setFirstName] = useState<string>('');
  const [username, setUsername] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [confirmPassword, setConfirmPassword] = useState<string>('');

  const {t, i18n} = useTranslation();

  const userRegister = () => {
    if (email.length > 0 && password.length > 0) {
      auth()
        .createUserWithEmailAndPassword(email, password)
        .then(() => {
          const currentUser = auth().currentUser;
          console.log(currentUser);
          if (currentUser) {
            return firestore()
              .collection('users')
              .doc(currentUser.uid)
              .set({
                name: name,
                firstname: firstname,
                username: username,
                bio: '',
                email: email,
                id: currentUser.uid,
                score: 0,
              })
              .then(() => {
                ToastAndroid.show(
                  'User account created & signed in!',
                  ToastAndroid.SHORT,
                );
              })
              .catch(error => {
                if (error.code === 'not-found') {
                  // Do nothing or handle it silently because it's an expected error
                } else {
                  // Handle other unexpected errors
                  console.error(error);
                  // Show a generic error message to the user
                  ToastAndroid.show(
                    'An error occurred. Please try again later.',
                    ToastAndroid.SHORT,
                  );
                }
              });
          }
        })
        .catch(error => {
          console.log(error);
          if (error.code === 'auth/email-already-in-use') {
            ToastAndroid.show(
              'That email address is already in use!',
              ToastAndroid.SHORT,
            );
            console.log('That email address is already in use!');
          }
          if (error.code === 'auth/invalid-email') {
            ToastAndroid.show(
              'That email address is invalid!',
              ToastAndroid.SHORT,
            );
            console.log('That email address is invalid!');
          }
          if (error.code === 'auth/weak-password') {
            ToastAndroid.show(
              'Password not strong enought',
              ToastAndroid.SHORT,
            );
            console.log('password weak !');
          }

          console.error(error);
        });
    } else {
      ToastAndroid.show('Please fill all the fields', ToastAndroid.SHORT);
    }
  };

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'transparent',
      color: '#3D3D3D',
    },
    box: {
      backgroundColor: '#e0e0e0',
      margin: 25,
      paddingBottom: 1,
      borderRadius: 10,
      width: '45%',
    },
    btn: {
      width: '90%',
      margin: 15,
      padding: 10,
      borderRadius: 10,
      backgroundColor: 'black',
      alignItems: 'center',
    },
    text: {
      fontSize: 16,
      color: '#FFFFFF',
    },
    inputContainer: {
      backgroundColor: '#e0e0e0',
      margin: 8,
      paddingHorizontal: 20,
      padding: 5,
      borderRadius: 10,
      width: '90%',
    },
    input: {
      fontSize: 16,
      color: '#000000',
    },
  });

  return (
    <KeyboardAwareScrollView>
      <View style={styles.container}>
        <View style={[styles.inputContainer]}>
        <TextInput
            value={name}
            onChangeText={innername => setName(innername)}
            style={styles.input}
            placeholder={t('lastName')}
          />

        </View>
        <View style={[styles.inputContainer]}>
        <TextInput
            value={firstname}
            onChangeText={innerfirstname => setFirstName(innerfirstname)}
            style={styles.input}
            placeholder={t('firstName')}
          />

        </View>
        <View style={[styles.inputContainer]}>
        <TextInput
            value={email}
            onChangeText={innermail => setEmail(innermail)}
            style={styles.input}
            placeholder={t('email')}
          />

        </View>
        <View style={styles.box} />
        <View style={[styles.inputContainer]}>
        <TextInput
            value={username}
            onChangeText={innerusername => setUsername(innerusername)}
            style={styles.input}
            placeholder={t('loginName')}
          />

        </View>
        <View style={[styles.inputContainer]}>
        <TextInput
    value={password}
    onChangeText={innerpassword => setPassword(innerpassword)}
    style={styles.input}
    placeholder={t('password')}
    secureTextEntry={true} // Masquer le mot de passe
  />

</View>

<View style={[styles.inputContainer]}>
<TextInput
    value={confirmPassword}
    onChangeText={innerconfirmPassword => setConfirmPassword(innerconfirmPassword)}
    style={styles.input}
    placeholder={t('confirmPass')}
    secureTextEntry={true} // Masquer le mot de passe de confirmation
  />

</View>

        <TouchableOpacity style={styles.btn} onPress={userRegister}>
          <Text style={styles.text}>{t('register')} </Text>
        </TouchableOpacity>
      </View>
    </KeyboardAwareScrollView>
  );
}

export default Register;
