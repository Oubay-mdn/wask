import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import FeedPost from '../components/FeedPost';
import HeaderTitleFeed from '../snippets/HeaderTitle';
import { useTranslation } from 'react-i18next';
import { useUser } from '../../config/UserContext';
import { WhoService } from '../services/games/who/whoService';
import { UserService } from '../services/userService';

const Feed = () => {
  const userService = new UserService();
  const whoService = new WhoService();
  const { user } = useUser();


  const [friendList, setFriendList] = useState<Friend[]>([]);
  const [answers, setAnswers] = useState<Answer[]>([]);
  const [socialMediaPosts, setSocialMediaPosts] = useState<SocialMediaPost[]>([]);

  useEffect(() => {
    getFriendsAndAnswers();
  }, []);

  const getFriendsAndAnswers = async () => {
    if (!user) return;
  
    try {
      // 1. Récupérer les amis et leurs réponses/images
      const friends = await userService.getUserFriends(user?.uid, ['id', 'firstname']);
      if (friends.length === 0) return;
  
      const { answers, pictures } = await whoService.getAllAnswers(friends.map(friend => friend.id));
  
      // 2. Créer les posts de réponses
      const answerPosts = answers.map(answer => {
        const friend = friends.find(f => f.id === answer.userId);
        return {
          user: friend?.firstname || 'Anonymous',
          pictureLink: '', // Pas d'image pour les réponses
          title: answer.type === 'who' ? '' : 'Réponse à une question',
          type: answer.type,
          comment: answer.comment || '',
          text: answer.type === 'who' ? answer.answer : '',
          author: answer.type === 'who' ? friend?.firstname || 'Anonymous' : '',
          ok: answer.type === 'who' && Array.isArray(answer.correctUserIds)
            ? answer.correctUserIds.includes(user?.uid || '')
            : false,
          createdAt: answer.timestamp
        };
      });
  
      // 3. Créer les posts des images
      const picturePosts = pictures.map(picture => {
        const friend = friends.find(f => f.id === picture.userId);
        return {
          user: friend?.firstname || 'Anonymous',
          pictureLink: picture.image.uri, // Image associée
          title: 'Photo partagée',
          type: 'image',
          comment: picture.comment,
          text: '', // Pas de texte pour les images
          author: friend?.firstname || 'Anonymous',
          ok: false, // Pas pertinent pour une image
          createdAt: picture.timestamp
        };
      });
  
      // 4. Fusionner et trier par date
      const combinedPosts = [...answerPosts, ...picturePosts].sort((a, b) => {
        const dateA = new Date(a.createdAt).getTime();
        const dateB = new Date(b.createdAt).getTime();
        
        // Si l'une des dates est invalide, on la considère comme ancienne
        if (isNaN(dateA)) return 1;
        if (isNaN(dateB)) return -1;
  
        return dateB - dateA; // Trier du plus récent au plus ancien
      });
  
      // 5. Mettre à jour l'état avec les données triées
      setSocialMediaPosts(combinedPosts);
      setFriendList(friends);
      setAnswers(answers);
    } catch (error) {
      console.error('Error fetching friends or answers:', error);
    }
  };

  const { t } = useTranslation();

  const styles = StyleSheet.create({
    FeedContainer: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      width: '100%',
      backgroundColor: 'transparent',
      gap: 20,
      marginTop: 40,
      paddingLeft: 25,
      paddingRight: 25,
      paddingBottom: 50, // Ajoute un espace en bas

    },
  });

  return (
    <View>
      <ScrollView contentContainerStyle={styles.FeedContainer}  
      contentInset={{ bottom: 50 }}
      keyboardShouldPersistTaps="handled"
      keyboardDismissMode="on-drag">
  <HeaderTitleFeed text={t('day')} />
  {socialMediaPosts.map((post, index) => (
    <FeedPost post={post} key={index} />
  ))}
</ScrollView>
    </View>
  );
};

export default Feed;

interface Friend {
  id: string;
  firstname: string;
}

interface Answer {
  id: string;
  userId: string;
  answer: string;
  type: string; // Type de réponse, par exemple 'who'
  comment: string,
  correctUserIds?: string[]; // Liste des utilisateurs ayant répondu correctement (si applicable)
}

interface SocialMediaPost {
  user: string; // Nom de l'utilisateur, toujours défini
  pictureLink: string; // Lien de l'image, toujours défini
  type: string; // Type de réponse, par exemple 'who'
  title: string; // Titre de la carte, toujours défini
  comment: string; // Commentaire de la carte, toujours défini
  text: string; // Texte pour les cartes 'who' (par défaut vide pour les autres)
  author: string; // Auteur pour les cartes 'who' (par défaut vide pour les autres)
  ok: boolean; // Indique si l'utilisateur est correct (par défaut false pour les autres)
  createdAt: string; 
}