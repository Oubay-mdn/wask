import { useState } from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  ToastAndroid,
  TextInput
} from 'react-native';
import Input from './../components/Input';
import showAlert from '../components/Alert';
import React from 'react';
import auth from '@react-native-firebase/auth';
import { useTranslation } from 'react-i18next';

function Login() {
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const { t, i18n } = useTranslation();

  const userLogin = () => {
    auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        ToastAndroid.show('User signed in!', ToastAndroid.SHORT);
        // navigation.navigate('profile')
      })
      .catch(error => {
        if (error.code === 'auth/wrong-password') {
          ToastAndroid.show('Wrong password', ToastAndroid.SHORT);
          console.log('no password matching this email');
        }

        if (error.code === 'auth/user-not-found') {
          ToastAndroid.show('User not found', ToastAndroid.SHORT);
          console.log('User not found');
        }

        console.error(error);
      });
  };
  const handleForgotPassword = () => {
    showAlert({
      title: t('passswordForget'),
      message: t('reinit'),
    });
  };
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'transparent',
      color: '#3D3D3D',
    },
    textmin: {
      fontSize: 16,
      color: '#FFFFFF',
    },
    box: {
      backgroundColor: '#e0e0e0',
      margin: 25,
      paddingBottom: 1,
      borderRadius: 10,
      width: '45%',
    },
    btn: {
      width: '90%',
      margin: 15,
      padding: 10,
      borderRadius: 10,
      backgroundColor: 'black',
      alignItems: 'center',
    },
    btn2: {
      width: '90%',
      borderRadius: 10,
      alignItems: 'center',
    },
    text: {
      fontSize: 16,
      color: '#FFFFFF',
    },
    inputContainer: {
      backgroundColor: '#e0e0e0',
      margin: 8,
      paddingHorizontal: 20,
      padding: 5,
      borderRadius: 10,
      width: '90%',
    },
    input: {
      fontSize: 16,
      color: '#000000',
    },
  });

  return (
    <View style={styles.container}>
      <Text style={styles.textmin}>{t('who')}</Text>
      <View style={styles.box} />
      <View style={[styles.inputContainer]}>
      <TextInput
          value={email}
          onChangeText={mail => setEmail(mail)}
          style={styles.input}
          placeholder={t('email')}
        />

      </View>
      <View style={[styles.inputContainer]}>
      <TextInput
          value={password}
          onChangeText={innerpassword => setPassword(innerpassword)}
          style={styles.input}
          placeholder={t('password')}
          secureTextEntry={true}
        />

      </View>
      <TouchableOpacity style={styles.btn2} onPress={handleForgotPassword}>
        <Text style={styles.text}>{t('passwordForget')}</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.btn} onPress={userLogin}>
        <Text style={styles.text}>{t('login')}</Text>
      </TouchableOpacity>
    </View>
  );
}

export default Login;
