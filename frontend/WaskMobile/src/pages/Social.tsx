import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput
} from 'react-native';
import {
  FlatList,
  GestureHandlerRootView,
} from 'react-native-gesture-handler';
import auth from '@react-native-firebase/auth';
import { useTranslation } from 'react-i18next';
import { UserService } from '../services/userService';
import Input from './../components/Input';

type Friend = {
  id: number;
  username: string;
  score: number;
};

function Social() {
  const { t, i18n } = useTranslation();
  const [friends, setFriends] = useState<Friend[]>([]);
  const [newFriendName, setNewFriendName] = useState('');
  const currentUser = auth().currentUser;
  const userService = new UserService(); // Create an instance of UserService
  const [user, setUser] = useState<any>({});
  const [msgAjout, setMsgAjout] = useState('');
  const [refreshKey, setRefreshKey] = useState(0); // Add this line

  useEffect(() => {
    const fetchUserData = async () => {
      if (currentUser) {
        try {
          // Charger les informations utilisateur
          const userData = await userService.get(currentUser.uid);
          setUser(userData);
  
          // Charger la liste des amis triée par score
          const userFriends = await userService.getUserFriends(currentUser.uid, [
            'id',
            'username',
            'score',
          ]);
          const sortedFriends = userFriends.sort((a, b) => b.score - a.score);
          setFriends(sortedFriends);
        } catch (error) {
          console.log('Error fetching data:', error);
        }
      } else {
        console.log('No user defined!');
      }
    };
  
    fetchUserData(); // Appeler la fonction asynchrone
  }, [refreshKey]);
  

  const addFriend = async () => {
    if (newFriendName === '') {
      setMsgAjout(t('enterName'));
      return;
    } else {
      const friendExists = await userService.searchUser(newFriendName); // Await the searchUser method to get the friend object
      console.log('user recherche', friendExists);
      if (friendExists === undefined) {
        setMsgAjout(t('unkonwn'));
        return;
      } else {
        console.log('id : ', currentUser?.uid ?? '', friendExists.id);
        userService
          .addFriend(currentUser?.uid ?? '', friendExists.id)
          .then(() => {
            setMsgAjout(t('addFriends'));
            setRefreshKey(oldKey => oldKey + 1); // Update the refreshKey state variable
          })
          .catch(error => {
            setMsgAjout(error.message);
          }); // Access the id property on the friend object
      }
    }
  };
  console.log(friends)

  return (
    <GestureHandlerRootView style={{ flex: 1 }}>
      <View style={styles.mainContainer}>
        <View style={styles.userBox}>
          <View style={styles.scoreUser}>
            <Text style={styles.h3}>{t('points')}</Text>
            <Text style={{ color: '#121212' }}>
              {t('collect')}
            </Text>
          </View>
          <View
            style={{
              width: '20%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={styles.h3}>{user?.score}</Text>
          </View>
        </View>

        <View style={{ width: '100%' }}>
          <Text style={styles.text}>{t('addAFriend')}</Text>
          <View style={[styles.inputContainer]}>
          <TextInput
              value={newFriendName}
              onChangeText={FriendName => setNewFriendName(FriendName)}
              placeholder={t('addAFriend')}
              style={styles.input}
            />

          </View>
          <TouchableOpacity style={styles.btn} onPress={addFriend}>
            <Text style={styles.text}>{t('add')}</Text>
          </TouchableOpacity>
          <Text style={{ textAlign: 'center' }}>{msgAjout}</Text>
        </View>

        <FlatList
          contentContainerStyle={{
            justifyContent: 'center',
            gap: 15,
          }}
          ListHeaderComponent={() => (
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Text style={styles.text}>{t('classement')}</Text>
              <TouchableOpacity>
                <Text style={styles.text}>X</Text>
              </TouchableOpacity>
            </View>
          )}
          scrollEnabled={true}
          style={styles.friendList}
          keyExtractor={item =>
            item.id ? item.id.toString() : Math.random().toString()
          }
          data={friends}
          renderItem={({ item, index }) =>
            item != undefined ? (
              <View
                style={[
                  styles.friendBox,
                  {
                    backgroundColor:
                      index === 0
                        ? '#0031AF'
                        : index === 1
                          ? '#99009C'
                          : index === 2
                            ? '#FA00FF'
                            : '#FFFFFFF',
                  },
                ]}>
                <Text
                  style={[
                    styles.text,
                    { color: index < 2 ? '#FFFFFF' : '#121212' },
                  ]}>
                  {item.username}
                </Text>
                <Text
                  style={[
                    styles.score,
                    { color: index < 2 ? '#FFFFFF' : '#121212' },
                  ]}>
                  {item.score}
                </Text>
              </View>
            ) : (
              <></>
            )
          }
        />
      </View>
    </GestureHandlerRootView>
  );
}

const styles = StyleSheet.create({
  userBox: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    height: 102,
    borderRadius: 15,
    width: '100%',
    paddingLeft: 20,
    paddingRight: 20,
  },
  h3: {
    fontSize: 15,
    fontWeight: 'bold',
    color: '#121212',
    fontFamily: 'Audiowide',
  },
  score: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#94059D',
    fontFamily: 'Audiowide',
  },
  scoreUser: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    gap: 10,
    width: '80%',
  },
  friendBox: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 66,
    borderRadius: 15,
    // shadowRadius: 10,
    // shadowColor: '#000',
    // shadowOpacity: 0.1,
    // shadowOffset: {width: 0, height: 2},
    paddingLeft: 20,
    paddingRight: 20,
  },
  mainContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    backgroundColor: 'transparent',
    gap: 20,
    marginTop: 40,
    paddingLeft: 25,
    paddingRight: 25,
  },
  friendList: {
    display: 'flex',
    flexDirection: 'column',

    width: '100%',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    color: '#3D3D3D',
  },
  box: {
    backgroundColor: '#e0e0e0',
    margin: 25,
    paddingBottom: 1,
    borderRadius: 10,
    width: '45%',
  },
  btn: {
    width: '90%',
    margin: 15,
    padding: 10,
    borderRadius: 10,
    backgroundColor: 'black',
    alignItems: 'center',
  },
  text: {
    fontSize: 16,
    color: '#FFFFFF',
  },
  input: {
    fontSize: 16,
    color: '#3D3D3D',
  },

  inputContainer: {
    backgroundColor: '#FFFFFF',
    margin: 8,
    paddingHorizontal: 20,
    padding: 5,
    borderRadius: 10,
    width: '90%',
  }
});

export default Social;
