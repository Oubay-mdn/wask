import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {useTranslation} from 'react-i18next';

const FeedPost = (props: {
  post: {
    user: string;
    pictureLink: string;
    title?: string; // Titre optionnel pour d'autres types de posts
    comment: string;
    type: string; // Ajout du champ type
    text?: string; // Champ spécifique pour les posts 'who'
    author?: string; // Auteur pour les posts 'who'
    ok?: boolean; // Statut spécifique pour les posts 'who'
  };
}) => {
  const {t} = useTranslation();
  const {post} = props;

  const styles = StyleSheet.create({
    postcontainer: {
      backgroundColor: 'white',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-evenly',
      alignItems: 'center',
      width: '100%',
      borderRadius: 15,
      shadowColor: '#000',
      shadowOpacity: 0.31,
      color:'black',
      shadowOffset: {width: 2, height: 4},
      shadowRadius: 3,
      padding: 5,
      marginBottom: 0,
    },
    postuser: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-start',
      gap: 15,
      color:'black',

      width: '100%',
      marginBottom: 5,
    },
    imagecontainer: {
      width: '100%',
      alignItems: 'center',
    },
    UserLogo: {
      width: 36,
      height: 36,
      borderRadius: 18,
      backgroundColor: 'grey',
    },
    usernametest: {
      fontFamily: 'Quicksand',
      fontWeight: '400',
      fontSize: 15,
      color:'black',
      lineHeight: 22,
      letterSpacing: -0.4,
    },
    commentSection: {
      marginTop: 10,
      textAlign: 'center',
    },
    textComment: {
      fontWeight: '400',
      fontSize: 13,
      fontFamily: 'Quicksand',
      textAlign: 'center',
      color:'black',

    },
    whoPostContainer: {
      backgroundColor: 'white',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-evenly',
      alignItems: 'center',
      width: '100%',
      borderRadius: 15,
      shadowColor: '#000',
      shadowOpacity: 0.31,
      shadowOffset: {width: 2, height: 4},
      shadowRadius: 3,
      padding: 15,
      marginBottom: 0,
    },
    whoText: {
      fontSize: 16,
      fontFamily: 'Quicksand',
      fontWeight: '800',
      marginBottom: 5,
      color:'black',

    },
    whoAuthor: {
      fontSize: 13,
      fontFamily: 'Quicksand',
      color: 'gray',

    },
    okWrapper:{
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-end',
      gap: 0,
      width: '100%',
      color:'black',
      marginBottom: 0,
    }


  });

  // Rendu conditionnel
  return (
    <View style={styles.postcontainer}>
      {/* Rendu pour les posts "photo" */}
      {post.type === 'image' && (
        <>
          <View style={styles.postuser}>
            <View style={styles.UserLogo} />
            <Text style={styles.usernametest}>{post.user}</Text>
          </View>
          <View style={styles.imagecontainer}>
            <Image
              source={{uri: post.pictureLink}}
              fadeDuration={300}
              style={{width: 310, height: 325, borderRadius: 15}}
              resizeMode="cover"
            />
          </View>
          <View style={styles.commentSection}>
            <Text style={styles.textComment}>{post.comment}</Text>
          </View>
        </>
      )}

      {/* Rendu pour les posts "who" */}
      {post.type === 'who' && (
  <View style={styles.whoPostContainer}>
    {/* User info */}
    <View style={styles.postuser}>
      <View style={styles.UserLogo} />
      <Text style={styles.usernametest}>{post.user}</Text>
    </View>

    {/* Main text */}
    <Text style={styles.whoText}>{post.text}</Text>
    
    {/* Optional comment */}
    <View style={styles.postuser}>
      <Text style={styles.textComment}>{post.comment}</Text>
    </View>
   

    {/* "OK" / "KO" positioned at the bottom right */}
    <View style={styles.okWrapper}>
      <Text style={styles.textComment}>{post.ok ? t('OK') : t('KO')}</Text>
    </View>
  </View>
)}
    </View>
  );
};

export default FeedPost;
