import {useContext} from 'react';
import React from 'react';
import {Text, StyleSheet} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Profil from '../pages/Profil';
import GameStack from '../components/gameComponents/GameStack';
import Feed from '../pages/Feed';
import HeaderTitleFeed from '../snippets/HeaderTitle';
import {IconFill} from '@ant-design/icons-react-native';
import Social from '../pages/Social';
import { useTranslation } from 'react-i18next';
const styles = StyleSheet.create({
  HeaderTitleFeedView: {
    flexDirection: 'row',
    alignItems: 'baseline',
    marginTop: 15,
  },
  HeaderTitleFeedImage: {width: 200, height: 50},
  HeaderTitleFeedtext: {
    fontFamily: 'Audiowide',
    fontWeight: '400',
    fontSize: 20,
    lineHeight: 22,
    color: 'white',
  },
  taabBar: {fontWeight: 'bold', color: '#FFFFFF'},
});
const TabBarLabel = ({text}: {text: string}) => (
  <Text style={styles.taabBar}>{text}</Text>
);
const getHeaderTitle = (text: string) => () => <HeaderTitleFeed text={text} />;
const getTabBarLabel = (text: string) => () => <TabBarLabel text={text} />;

function LoggedNav() {
  /* const Icon = (props: { name: string, focused?: boolean }) => {
    const color = props.focused ? "#FA00FF" : "#FFFFFF";
    return <FontAwesomeIcon name={props.name} size={20} color={color} />;
  };*/
  const Tab = createBottomTabNavigator();
  const {t, i18n} = useTranslation();

  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        headerTitle: getHeaderTitle(''),
        tabBarStyle: {
          backgroundColor: '#99009C',
          borderTopWidth: 0,
        },

        tabBarLabel: getTabBarLabel(t(route.name)),
        headerStyle: {
          backgroundColor: 'transparent',
          borderBottomWidth: 0,
          elevation: 0,
          shadowColor: 'transparent',
        },
        headerTitleAlign: 'center',
        headerTintColor: '#FFFFFF',
        headerShown: false,
      })}>
      <Tab.Screen
        name={t('navigation.profile')}
        component={Profil}
        options={() => ({
          showIcon: true,

          tabBarIcon: () => <IconFill name="profile" />,
        })}
      />
      <Tab.Screen
        name={t('navigation.GameStack')}
        component={GameStack}
        options={() => ({
          showIcon: true,

          tabBarIcon: () => <IconFill name="dashboard" />,
        })}
      />
      <Tab.Screen
        name={t('navigation.feed')}
        component={Feed}
        // This changes the header title for the Feed screen

        options={() => ({
          showIcon: true,
          tabBarIcon: () => <IconFill name="home" />,
        })}
      />
      <Tab.Screen
        name={t('navigation.social')}
        component={Social}
        // This changes the header title for the Feed screen

        options={() => ({
          showIcon: true,
          tabBarIcon: () => <IconFill name="home" />,
        })}
      />
    </Tab.Navigator>
  );
}

export default LoggedNav;
