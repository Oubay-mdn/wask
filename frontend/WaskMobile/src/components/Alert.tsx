import {Alert as RNAlert} from 'react-native';

interface AlertProps {
  title: string;
  message: string;
}

const showAlert = ({title, message}: AlertProps): void => {
  RNAlert.alert(title, message);
};

export default showAlert;
