import React from 'react';
import { View, TextInput, StyleSheet, StyleProp, ViewStyle } from 'react-native';

interface InputProps {
  text: string;
  value: string;
  onChange: (text: string) => void;
  style?: StyleProp<ViewStyle>;
  secure?: boolean;
}

const Input: React.FC<InputProps> = ({ text, style, secure, onChange }) => {
  return (
    <View style={[styles.inputContainer, style]}>
      <TextInput secureTextEntry={secure} style={styles.input} placeholder={text} onChangeText={(e) => onChange(e)} />
    </View>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    backgroundColor: '#e0e0e0',
    margin: 2,
    paddingHorizontal: 20,
    padding: 0,
    borderRadius: 10,
    width: '90%',
  },
  input: {
    fontSize: 16,
    color: '#3D3D3D',
  },
});

export default Input;
