import React, { useEffect, useState } from 'react';
import { useUser } from './../../config/UserContext';
import Input from '../components/Input';
import { firebase } from '@react-native-firebase/auth';
import { FirebaseFirestoreTypes } from '@react-native-firebase/firestore';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
  ScrollView,
  Platform
} from 'react-native';
import {
  ImagePickerResponse,
  launchCamera,
  launchImageLibrary,
} from 'react-native-image-picker';
import { PictureService } from './../services/games/pictureService';
import { ImageLibraryOptions } from 'react-native-image-picker';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';


function UploadPicture() {
  const navigation = useNavigation<any>();
  const { t, i18n } = useTranslation();
  const { user } = useUser();
  const [image, setImage] = useState<ImagePickerResponse>(
    {} as ImagePickerResponse,
  );
  const pictureService = new PictureService();
  const [comment, setComment] = useState<string>('');
  const [userProfile, setUserProfile] = useState<
    FirebaseFirestoreTypes.DocumentData | undefined
  >(undefined);


  async function getPhoto() {
    const options: ImageLibraryOptions = {
      mediaType: 'photo',
      quality: 1,
    };
    const result = await launchImageLibrary(options);
    console.log(result);
    setImage(result);
  }

  async function takePhoto() {
    const options: ImageLibraryOptions = {
      mediaType: 'photo',
      quality: 1,
    };
    const result = await launchCamera(options);
    console.log(result);
    setImage(result);
  }


  async function addPicture() {
    const imageUri = getImageUri();
    const assets = image.assets;
    
    if (!imageUri || !assets) {
      console.error('Image is missing');
      return;
    }
  
    const id = userProfile?.id; // Vérifie que l'ID utilisateur est défini
    if (!id) {
      console.error('User profile ID is undefined');
      return;
    }
  
    try {
      const image = assets[0];
      const data = {
        image: {
          uri: Platform.OS === 'android'
            ? image.uri
            : image.uri!.replace('file://', ''), // Corrige l'URI pour iOS
          name: image.fileName,
          type: image.type,
        },
        compression: true,
      };
  
      // Appel à la méthode `createPicture`
      await pictureService.createPicture(comment, id, data);
  
      console.log('Picture successfully added!');
      navigation.navigate('SwipePicture' as never);
    } catch (e) {
      console.error('Error while adding picture:', e);
    }
  
    deletePhoto();
  }
  

  function getImageUri() {
    return image && image.assets && image.assets[0].uri;
  }

  function deletePhoto() {
    setImage({} as ImagePickerResponse);
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: 'transparent',
      color: '#3D3D3D',
      padding: 20,
    },
    btn: {
      width: '90%',
      margin: 15,
      padding: 10,
      borderRadius: 10,
      backgroundColor: 'black',
      alignItems: 'center',
    },
    text: {
      fontSize: 16,
      color: '#FFFFFF',
    },
    formContainer: {
      width: '100%',
      position: 'relative',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 20,
    },
    deleteButtonContainer: {
      height: 40,
      top: -20,
      width: 190,
      position: 'absolute',
      display: 'flex',
      alignItems: 'flex-end',
      zIndex: 2,
    },
    deleteButton: {
      width: 40,
      height: 40,
      borderRadius: 50,
      backgroundColor: 'red',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      left: 0,
    },
    image: {
      width: 150,
      height: 150,
      marginBottom: 20,
    },
    checkboxContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-start',
      width: '100%',
      margin: 8,
      paddingHorizontal: 20,
      padding: 5,
    },
    checkbox: {
      height: 30,
      width: 30,
    },
    checkboxLabel: {
      fontSize: 16,
      marginLeft: 10,
      color: 'black',
    }
  });


  useEffect(() => {
    const fetchUserInfo = async () => {
      if (!user) {
        return;
      }
      const { uid } = user;
      // Discard fetch when user ID not defined
      if (!uid) {
        return;
      }
      firebase
        .firestore()
        .collection('users')
        .doc(uid)
        .get()
        .then(doc => {
          const data = doc.data();
          if (data) {
            setUserProfile(data);
          } else {
            // handle the case where the data is undefined
            console.error('No such document!');
          }
        });
    };

    fetchUserInfo();
  }, [user]);

  return (
    <ScrollView>
      <View style={styles.container}>
        { }
        {image && image.assets && image.assets[0] ? (
          <View style={styles.formContainer}>
            <View style={styles.deleteButtonContainer}>
              <TouchableOpacity
                onPress={deletePhoto}
                style={styles.deleteButton}>
              </TouchableOpacity>
            </View>
            <Image source={{ uri: getImageUri() }} style={styles.image} />
            <Input
              value={comment}
              onChange={commentset => setComment(commentset)}
              text={t('tonComment')}
            />
            <TouchableOpacity style={styles.btn} onPress={addPicture}>
              <Text style={styles.text}>{t('addPhoto')}</Text>
            </TouchableOpacity>
          </View>
        ) : (
          <>
            <TouchableOpacity style={styles.btn} onPress={takePhoto}><Text style={styles.text}>{t('takePicture')}</Text></TouchableOpacity>
            <TouchableOpacity style={styles.btn} onPress={getPhoto} ><Text style={styles.text}>{t('importPicture')}</Text></TouchableOpacity>
          </>
        )}
      </View>
    </ScrollView>
  );
}

export default UploadPicture;