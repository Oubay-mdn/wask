import {useContext} from 'react';
import React from 'react';
import {Text} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
//import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import Profil from '../pages/Profil';
import Login from '../pages/Login';
import Register from '../pages/Register';
import {useTranslation} from 'react-i18next';

// const styles = StyleSheet.create({
//   colorStyle: {fontWeight: 'bold', color: color},
// });
const getTabBarLabel =
  (text: string) =>
  ({focused}: {focused: boolean}) => {
    let color = focused ? '#FA00FF' : '#FFFFFF';
    return <TabBarLabel text={text} color={color} />;
  };

// Inside TabBarLabel component
const TabBarLabel = ({text, color}: {text: string; color: string}) => (
  // eslint-disable-next-line react-native/no-inline-styles
  <Text style={{fontWeight: 'bold', color: color}}>{text}</Text>
);
function LoginNav() {
  // const Icon = (props: { name: string, focused: boolean }) => {
  // const color = props.focused ? "#3D3D3D" : "#FFFFFF";
  // return <FontAwesomeIcon name={props.name} size={20} color={color} />;
  //};
  const Tab = createBottomTabNavigator();
  const {t, i18n} = useTranslation();
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        headerTitle: t(route.name),
        tabBarStyle: {
          backgroundColor: '#3D3D3D',
          borderTopWidth: 0,
        },
        tabBarLabel: getTabBarLabel(t(route.name)),

        headerStyle: {
          backgroundColor: '#FA00FF',
          borderBottomWidth: 0,
        },
        headerTitleAlign: 'center',
        headerTintColor: '#3D3D3D',
      })}>
      <Tab.Screen name={t('navigation.profile')} component={Profil} />
      <Tab.Screen
        name={t('navigation.login')}
        component={Login}
        /* options={() => ({
          tabBarIcon: ({ focused }) => (
            <Icon name={"user-plus"} focused={focused} />
          ),
        })}*/
      />
      <Tab.Screen
        name={t('navigation.register')}
        component={Register}
        /* options={() => ({
          tabBarIcon: ({ focused }) => (
            <Icon name={"user-plus"} focused={focused} />
          ),
        })}*/
      />
    </Tab.Navigator>
  );
}

export default LoginNav;
