import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Button,
  SafeAreaView,
} from 'react-native';
import Input from '../Input';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';
import HeaderTitleFeed from '../../snippets/HeaderTitle';
import LinearGradient from 'react-native-linear-gradient';
import { BlurView } from '@react-native-community/blur';
import { WhoService } from '../../services/games/who/whoService';
import { useUser } from '../../../config/UserContext';
import { WhoQuestionService } from '../../services/games/who/whoQuestionService';

const SwipeAnswer = () => {
  const whoService = new WhoService();
  const WhoQService = new WhoQuestionService();

  const navigation = useNavigation<any>();
  const [question, setQuestion] = useState<string>('La question du jour ?');
  const [answer, setAnswer] = useState<string>('');
  const [comment, setComment] = useState<string>('');

  const [seconds, setSeconds] = useState(5); // 10 minutes in seconds
  const [isRunning, setIsRunning] = useState(false);
  const [isTimeOver, setIsTimeOver] = useState(false);

  const { user } = useUser();

  useEffect(() => {
    // whoService.startGame();
    getQuestion();
  }, []);
  const getQuestion = async () => {
    const whoQuestionService = new WhoQuestionService(); // Créer une instance de la classe
    const questionSnapshot = await whoQuestionService.getLastQuestion(); // Appeler la méthode sur l'instance

    if (!questionSnapshot.empty) {
      const question = questionSnapshot.docs[0].data(); // Récupérer les données de la question
      setQuestion(question.text); // Mettre à jour l'état avec la question récupérée
    } else {
      console.log('No question found');
    }
  };


  useEffect(() => {
    let timer: string | number | NodeJS.Timeout | undefined;
    if (isRunning && seconds > 0) {
      timer = setInterval(() => {
        setSeconds(prevSeconds => prevSeconds - 1);
      }, 1000);
    } else if (seconds === 0) {
      setIsTimeOver(true);
      setIsRunning(false);
    }

    return () => clearInterval(timer);
  }, [isRunning, seconds]);

  const { t, i18n } = useTranslation();
  const handleStart = () => setIsRunning(true);
  const handleStop = () => setIsRunning(false);
  const handleReset = () => {
    setIsRunning(false);
    setSeconds(600);
    setIsTimeOver(false);
  };

  const formatTime = (seconds: number) => {
    const minutes = Math.floor(seconds / 60);
    const remainingSeconds = seconds % 60;
    return `${minutes}:${remainingSeconds < 10 ? '0' : ''}${remainingSeconds}`;
  };

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      backgroundColor: 'transparent',
      color: '#3D3D3D',
      width: '100%',
      gap: 20,
      marginTop: 40,
    },
    QuestionView: {
      width: '100%',
      height: '6%',
      marginBottom: 50,
      zIndex: -1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'transparent',
      position: 'relative',
    },
    gradientOverlay: {
      position: 'absolute',
      width: '100%',
      height: '100%',
      zIndex: -1,
    },
    questionText: {
      fontSize: 24,
      fontWeight: 'bold',
      fontFamily: 'Audiowide-Regular',
      color: '#FFFFFF',
      textShadowColor: 'rgba(0, 0, 0, 0.75)',
      textShadowOffset: { width: 0, height: 2 },
      textShadowRadius: 3,
    },
    title: {
      fontSize: 30,
    },
    view: {
      padding: 10,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 12,
      },
      shadowOpacity: 0.58,
      shadowRadius: 16.0,
      elevation: 24,
    },
    absolute: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
    },
    globalStyle: {
      backgroundColor: 'transparent',
      width: '100%',
      color: '#3D3D3D',
    },
    textContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    timer: {
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      backgroundColor: 'transparent',
      paddingHorizontal: 20,
      height: '30%',
    },
    timerTextContainer: {
      borderRadius: 10,
      overflow: 'hidden',
      position: 'relative',
      padding: 10,
      backgroundColor: 'rgba(255, 255, 255, 0.8)',
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.8,
      shadowRadius: 20,
    },
    timerText: {
      fontSize: 30,
      fontFamily: 'Audiowide-Regular',
      color: '#3D3D3D', // Couleur du texte
      textAlign: 'center',
    },
    timeOverText: {
      fontSize: 24,
      color: 'red',
      marginBottom: 20,
      fontFamily: 'Audiowide-Regular',
    },
    inputContainer: {
      backgroundColor: '#e0e0e0',
      margin: 15,
      paddingHorizontal: 20,
      padding: 5,
      borderRadius: 10,
      width: '90%',
    },
    btn: {
      width: '90%',
      margin: 15,
      marginTop: 50,
      padding: 10,
      borderRadius: 10,
      backgroundColor: 'black',
      alignItems: 'center',
    },
  });

  return (
    <View style={styles.container}>
      <HeaderTitleFeed text={''} />
      <View style={styles.globalStyle}>
        <View style={styles.QuestionView}>
          <LinearGradient
            colors={['rgba(255, 255, 255, 0)', 'rgba(255, 255, 255, 0.3)']}
            style={styles.gradientOverlay}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
          />
          <View style={styles.textContainer}>
            <Text style={styles.questionText}>{question}</Text>
          </View>
        </View>
        <View style={styles.timer}>
          {/* {!isTimeOver && (
            <SafeAreaView
              // eslint-disable-next-line react-native/no-inline-styles
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                width: '100%',
              }}>
              <View
                // eslint-disable-next-line react-native/no-inline-styles
                style={{
                  width: 100,
                  height: 40,
                  borderRadius: 30,
                  position: 'absolute',
                  backgroundColor: 'rgba(200,200,200,0.8)',
                }}
              />
              <BlurView
                style={styles.absolute}
                blurType="light"
                blurAmount={10}
                blurRadius={9}
              />
              <View style={styles.view}>
                <Text style={styles.title}>{formatTime(seconds)}</Text>
              </View>
            </SafeAreaView>
          )}
          {isTimeOver && <Text style={styles.timeOverText}>Trop tard</Text>}
          <Button onPress={handleStart} title="Start" />
          <Button onPress={handleStop} title="Stop" />
          <Button onPress={handleReset} title="Reset" /> */}
        </View>
        <View style={[styles.inputContainer]}>
          <Input
            value={answer}
            onChange={inneranswer => setAnswer(inneranswer)}
            text={t('response')}
          />
        </View>
        <Text style={{ color: 'white', paddingLeft: 10, paddingTop: 10 }}>
          {t('comment')}
        </Text>
        <View style={[styles.inputContainer]}>
          <Input
            value={comment}
            onChange={innercomment => setComment(innercomment)}
            text={t('tonComment')}
          />
        </View>
        {!isTimeOver && (
          <TouchableOpacity
            style={styles.btn}
            onPress={() => {
              if (!user) return;
              whoService.postAnswer(user?.uid, answer,comment);
              navigation.navigate('SwipeAnswer' as never);
            }}>
            <Text
              style={{
                fontSize: 16,
                color: '#FFFFFF',
              }}>
              {t('valider')}
            </Text>
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

export default SwipeAnswer;
