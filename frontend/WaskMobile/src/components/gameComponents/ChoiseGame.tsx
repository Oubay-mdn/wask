import React, {useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useTranslation} from 'react-i18next';
import HeaderTitleFeed from '../../snippets/HeaderTitle';
import LinearGradient from 'react-native-linear-gradient';

const ChoiseGame = () => {
  const navigation = useNavigation<any>();
  const {t, i18n} = useTranslation();
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      backgroundColor: 'transparent',
      color: '#3D3D3D',
      width: '100%',
      gap: 20,
      marginTop: 40,
    },
    QuestionView: {
      width: '100%',
      height: '6%',
      marginBottom: 50,
      zIndex: -1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'transparent',
      position: 'relative',
    },
    gradientOverlay: {
      position: 'absolute',
      width: '100%',
      height: '100%',
      zIndex: -1,
    },
    questionText: {
      fontSize: 24,
      fontWeight: 'bold',
      color: '#FFFFFF',
      textShadowColor: 'rgba(0, 0, 0, 0.75)',
      textShadowOffset: {width: 0, height: 2},
      textShadowRadius: 3,
    },
    globalStyle: {
      width: '100%',
      height: '100%',
      backgroundColor: 'transparent',
      color: '#3D3D3D',
    },
    // style for containe text or image or a circle on the middle of the container
    textContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    cardText: {
      fontSize: 20,
      fontWeight: 'bold',
      color: '#3D3D3D',
    },
    SwipeContainerLeft: {
      position: 'relative',
      borderRadius: 15,
      width: '100%',
      height: '100%',
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 1 },  // Ombre qui va vers le haut (valeur négative pour height)
      shadowOpacity: 0.3,
      shadowRadius: 5,  // Rayon pour une ombre moins floue
      elevation: 15,  // Pour Android, si tu veux que l'ombre soit projetée vers le haut
      transform: [{ rotateY: '40deg' }],
    },
    SwipeContainerRight: {
      position: 'relative',
      borderRadius: 15,
      width: '100%',
      height: '100%',
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 1 },  // Ombre qui va vers le haut (valeur négative pour height)
      shadowOpacity: 0.3,
      shadowRadius: 5,  // Rayon pour une ombre moins floue
      elevation: 15,// Réduit l'élévation pour Android
      transform: [{ rotateY: '-40deg' }],
    },
    cardContainer: {
      width: '50%',
      height: '70%',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'transparent',
      maxWidth: '100%',
      zIndex: 99,
      padding: 10,
    },
    swipeNativeStyle: {
      width: '100%',
      height: '100%',
      backgroundColor: 'transparent',
      flexDirection: 'column',
      justifyContent: 'space-evenly',
      alignItems: 'center',
      top: 0,
      left: 0,
    },
    choise: {
      flexDirection: 'row',
      justifyContent: 'space-evenly',
      alignItems: 'center',
    }
  });

  return (
    <View style={styles.container}>
      <HeaderTitleFeed text={''} />
      <View style={styles.globalStyle}>
        <View style={styles.QuestionView}>
          {/* Contenu de la vue de question */}
          <LinearGradient
            colors={['rgba(255, 255, 255, 0)', 'rgba(255, 255, 255, 0.3)']} // Gradient colors
            style={styles.gradientOverlay}
            start={{x: 0, y: 0}} // Start from left
            end={{x: 1, y: 0}} // End at right
          />
          <View style={styles.textContainer}>
            <Text style={styles.questionText}>{t('theme')}</Text>
          </View>
        </View>
        <View style={styles.choise}>
        <View style={styles.cardContainer}>
          <TouchableOpacity
            style={styles.SwipeContainerLeft}
            onPress={() => {
              navigation.navigate('UploadPicture' as never);
            }}>
            <LinearGradient
              colors={['#00008B', '#8A2BE2']} // Dégradé de bleu foncé à violet
              style={styles.swipeNativeStyle}
              start={{x: 0, y: 0}} // Start from top
              end={{x: 0, y: 1}} // End at bottom
            >
              <Image
                source={require('../../../assets/images/Vector.png')}
              />
            </LinearGradient>
          </TouchableOpacity>
        </View>
        <View style={styles.cardContainer}>
          <TouchableOpacity
            style={styles.SwipeContainerRight}
            onPress={() => {
              navigation.navigate('Answer' as never);
            }}>
            <LinearGradient
              colors={['#8A2BE2', '#2E0854']} // Dégradé de violet à violet foncé presque noir
              style={styles.swipeNativeStyle}
              start={{ x: 0, y: 0 }} // Start from top
              end={{ x: 0, y: 1 }} // End at bottom
            >
              <Image
                source={require('../../../assets/images/Search.png')}
              />
            </LinearGradient>
          </TouchableOpacity>
        </View>
        </View>
      </View>
    </View>
  );
};

export default ChoiseGame;
