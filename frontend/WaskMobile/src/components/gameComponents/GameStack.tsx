import {createStackNavigator} from '@react-navigation/stack';
import ChoiseGame from './ChoiseGame';
import SwipeAnswer from './SwipeAnswer';
import SwipePicture from './SwipePicture';
import Answer from './Answer';
import UploadPicture from '../UploadPicture';

const Stack = createStackNavigator();

function MainStack() {
  return (
    <Stack.Navigator initialRouteName="ChoiseGame">
      <Stack.Screen
        options={({route}) => ({
          headerTitle: '',
          headerTransparent: true,
          headerTintColor: '#FFFFFF',
        })}
        name="ChoiseGame"
        component={ChoiseGame}
      />
      <Stack.Screen
        options={({route}) => ({
          headerTitle: '',
          headerTransparent: true,
          headerTintColor: '#FFFFFF',
        })}
        name="SwipeAnswer"
        component={SwipeAnswer}
      />
      <Stack.Screen
        options={({route}) => ({
          headerTitle: '',
          headerTransparent: true,
          headerTintColor: '#FFFFFF',
        })}
        name="SwipePicture"
        component={SwipePicture}
      />
      <Stack.Screen
        options={({route}) => ({
          headerTitle: '',
          headerTransparent: true,
          headerTintColor: '#FFFFFF',
        })}
        name="Answer"
        component={Answer}
      />
      <Stack.Screen
        options={({route}) => ({
          headerTitle: '',
          headerTransparent: true,
          headerTintColor: '#FFFFFF',
        })}
        name="UploadPicture"
        component={UploadPicture}
      />
    </Stack.Navigator>
  );
}

export default MainStack;
