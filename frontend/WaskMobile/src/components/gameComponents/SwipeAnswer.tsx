import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Swiper from 'react-native-deck-swiper';
import {LinearGradient} from 'react-native-linear-gradient'; // Import LinearGradient
import HeaderTitleFeed from '../../snippets/HeaderTitle';
import {UserService} from '../../services/userService';
import {WhoService} from '../../services/games/who/whoService';
import {useUser} from '../../../config/UserContext';
import {useNavigation} from '@react-navigation/native';
import Feed from '../../pages/Feed';
import { WhoQuestionService } from '../../services/games/who/whoQuestionService';
const SwipeAnswer = () => {
  const userService = new UserService();
  const whoService = new WhoService();
  const navigation = useNavigation<any>();
  const [question, setQuestion] = useState<string>('La question du jour ?');

  const {user} = useUser();

  const [cards, setCards] = useState<Card[]>([]);
  const [friendList, setFriendList] = useState<Friend[]>([]);
  const [answers, setAnswers] = useState<Answer[]>([]);

  const [actualCard, setActualCard] = useState(0);

  const styles = StyleSheet.create({
    GlobalContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      backgroundColor: 'transparent',
      color: '#3D3D3D',
      width: '100%',
      gap: 20,
      marginTop: 40,
    },
    container: {
      flexDirection: 'row', // Utilisez une disposition en ligne pour organiser les éléments horizontalement
      justifyContent: 'center',
      backgroundColor: 'transparent',
      color: '#3D3D3D',
    },
    leftView: {
      // width: '20%', // 20% de la largeur pour la vue de gauche
      flex: 15,
      backgroundColor: 'transparent',
      zIndex: 1,
    },
    rightView: {
      // width: '20%', // 20% de la largeur pour la vue de droite
      flex: 15,
      backgroundColor: 'transparent',
      zIndex: 1,
    },
    topView: {
      width: '100%', // 100% de la largeur pour la vue de haut
      height: '15%', // 20% de la hauteur pour la vue de haut
      backgroundColor: 'transparent',
      zIndex: -1,
      alignItems: 'center', // Alignez le contenu au centre
      justifyContent: 'center', // Centrez le contenu verticalement
    },
    circle: {
      width: 60,
      height: 60,
      borderRadius: 30,
      backgroundColor: 'white',
      marginBottom: 10,
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 2},
      shadowOpacity: 0.8,
      shadowRadius: 2,
      elevation: 5,
    },
    QuestionView: {
      width: '100%',
      height: '6%',
      marginBottom: 50,
      zIndex: -1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'transparent',
      position: 'relative',
    },
    gradientOverlay: {
      position: 'absolute',
      width: '100%',
      height: '100%',
      zIndex: -1,
    },
    questionText: {
      fontSize: 24,
      fontWeight: 'bold',
      color: '#FFFFFF',
      textShadowColor: 'rgba(0, 0, 0, 0.75)',
      textShadowOffset: {width: 0, height: 2},
      textShadowRadius: 3,
    },
    friendNameText: {
      fontSize: 15,
      fontWeight: '300',
      color: '#FFFFFF',
      textShadowColor: 'rgba(0, 0, 0, 0.75)',
      textShadowOffset: {width: 0, height: 1},
      textShadowRadius: 2,
    },
    swiperContainer: {
      width: '50%', // 50% de la largeur pour la balise Swiper
      // flex : 60,
      height: '100%', // Ajuster la hauteur selon vos besoins
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'transparent',
      maxWidth: '100%',
      zIndex: 99,
    },
    cardText: {
      fontSize: 20,
      fontWeight: 'bold',
      color: '#3D3D3D',
    },
    cardContainer: {
      backgroundColor: 'white',
      flexDirection: 'column',
      justifyContent: 'space-evenly',
      alignItems: 'center',
      position: 'relative',
      width: '100%',
      height: '100%',
      borderRadius: 15,
      shadowColor: '#000',
      shadowOpacity: 0.31,
      shadowOffset: {width: 2, height: 4},
      shadowRadius: 3,
    },
    swipeNativeStyle: {
      width: '100%',
      height: '100%',
      backgroundColor: 'transparent',
      top: 0,
      left: 0,
    },
    globalStyle: {
      width: '100%',
      height: '100%',
      backgroundColor: 'transparent',
      color: '#3D3D3D',
    },
    // style for containe text or image or a circle on the middle of the container
    textContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
  });

  useEffect(() => {
    getFriends();
    getQuestion();

  }, []);

  useEffect(() => {
    if (!friendList.length || !answers.length) return;
    getShuffledFriends();
  }, [friendList, answers]);

  const getFriends = async () => {
    if (!user) return;
    const friends = await userService.getUserFriends(user?.uid, [
      'id',
      'firstname',
    ]);
    const answers = await whoService.getAnswers(
      friends.map(friend => friend.id),
    );
    setAnswers(answers);
    setFriendList(friends);
  };

  const getQuestion = async () => {
      const whoQuestionService = new WhoQuestionService(); // Créer une instance de la classe
      const questionSnapshot = await whoQuestionService.getLastQuestion(); // Appeler la méthode sur l'instance
      
      if (!questionSnapshot.empty) {
        const question = questionSnapshot.docs[0].data(); // Récupérer les données de la question
        setQuestion(question.text); // Mettre à jour l'état avec la question récupérée
      } else {
        console.log('No question found');
      }
  };

  const shuffle = (arr: any[]) => arr.sort(() => Math.random() - 0.5);
  const getShuffledFriends = async () => {
    if (!user || !answers) return;
    const shuffledFriends = answers.map(answer => {
      const goodUser = friendList.find(friend => friend.id === answer.userId);
      if (!goodUser) return;
      const filteredFriends = friendList.filter(
        friend => friend.id !== goodUser.id,
      );
      return shuffle(filteredFriends).slice(0, 2).concat(goodUser);
    });
    getCards(shuffledFriends);
  };

  const getCards = async (shuffledFriends: any[]) => {
    const formattedCards = answers.map((answer, i) => {
      const friend = friendList.find(
        friend => friend.id === answer.userId,
      ) as Friend;
      return {
        text: answer.answer,
        author: friend,
        shuffledFriends: shuffledFriends[i],
        answerId: answer.id,  // Ajouter l'ID de la réponse
        comment: answer.comment || ''
      };
    });
    if (!formattedCards) return;
    setCards(formattedCards);
  };
  return (
    <View style={styles.GlobalContainer}>
      <HeaderTitleFeed text={''} />
      {cards.length > 0 && (
        <View style={styles.globalStyle}>
          <View style={styles.QuestionView}>
            {/* Contenu de la vue de question */}
            <LinearGradient
              colors={['rgba(255, 255, 255, 0)', 'rgba(255, 255, 255, 0.3)']} // Gradient colors
              style={styles.gradientOverlay}
              start={{x: 0, y: 0}} // Start from left
              end={{x: 1, y: 0}} // End at right
            />
            <View style={styles.textContainer}>
              <Text style={styles.questionText}>{question}</Text>
            </View>
          </View>
          <View style={styles.topView}>
            {/* Contenu de la vue du haut */}
            <View style={styles.textContainer}>
              {/* add the circle */}
              <View style={styles.circle} />
              <Text style={styles.friendNameText}>Top Friend</Text>
              <Text style={styles.friendNameText}>
                {cards[actualCard].shuffledFriends[0].firstname || 'Ami manquant'}
              </Text>
            </View>
          </View>
          <View style={styles.container}>
            <View style={styles.leftView}>
              {/* Contenu de la vue de gauche */}
              <View style={styles.textContainer}>
                <View style={styles.circle} />
                <Text style={styles.friendNameText}>Left Friend</Text>
                <Text style={styles.friendNameText}>
                  {cards[actualCard].shuffledFriends[1].firstname || 'Ami manquant'}
                </Text>
              </View>
            </View>

            <View style={styles.swiperContainer}>
              <Swiper
                cardStyle={styles.swipeNativeStyle}
                cards={cards}
                renderCard={card => (
                  <View style={styles.cardContainer}>
                    <Text style={styles.cardText}>{card.text}</Text>
                    <Text style={styles.cardText}>{card.comment}</Text>
                  </View>
                )}
                onSwipedLeft={cardIndex => {
                  if (!user) return;
                  whoService.ValidateFriend(
                    cards[cardIndex].shuffledFriends[1].id ===
                      cards[cardIndex].author.id,
                      user.uid,
                      cards[cardIndex].answerId,  // Utilisation de l'ID de la réponse
                  );
                }}
                onSwipedRight={cardIndex => {
                  if (!user) return;
                  whoService.ValidateFriend(
                    cards[cardIndex].shuffledFriends[2].id ===
                      cards[cardIndex].author.id,
                    user.uid,
                    cards[cardIndex].answerId,  // Utilisation de l'ID de la réponse

                  );
                }}
                onSwipedTop={cardIndex => {
                  if (!user) return;
                  whoService.ValidateFriend(
                    cards[cardIndex].shuffledFriends[0].id ===
                      cards[cardIndex].author.id,
                    user.uid,
                    cards[cardIndex].answerId,  // Utilisation de l'ID de la réponse

                  );
                }}
                onSwiped={(cardIndex): void => {
                  console.log(cardIndex);
                  console.log(cards[cardIndex]);
                  console.log('Nombre de carte swipe : ', cardIndex + 1);
                  if (cardIndex + 1 < cards.length) {
                    setActualCard(cardIndex + 1);
                  }
                }}
                onSwipedAll={() => {
                  console.log('Toutes les cartes sont swipe');
                  navigation.navigate('Feed');
                }}
                cardIndex={0}
                backgroundColor={'transparent'}
                stackSize={2}
                disableBottomSwipe={true}
              />
            </View>

            <View style={styles.rightView}>
              {/* Contenu de la vue de droite */}
              <View style={styles.textContainer}>
                <View style={styles.circle} />
                <Text style={styles.friendNameText}>Right Friend</Text>
                <Text style={styles.friendNameText}>
                  {cards[actualCard].shuffledFriends[2].firstname || 'Ami manquant'}
                </Text>
              </View>
            </View>
          </View>
        </View>
      )}
    </View>
  );
};

export default SwipeAnswer;

interface Card {
  text: string;
  author: Friend;
  shuffledFriends: Friend[];
  answerId: string;  // Ajoutez l'ID de la réponse
  comment:string

}

interface Friend {
  id: string;
  firstname: string;
}

interface Answer {
  id: string;
  userId: string;
  answer: string;
  comment:string
}
