import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import Swiper from 'react-native-deck-swiper';
import HeaderTitleFeed from '../../snippets/HeaderTitle';
import { PictureService } from './../../services/games/pictureService';
import { UserService } from '../../services/userService';
import { WhoService } from '../../services/games/who/whoService';
import { useUser } from '../../../config/UserContext';
import { useNavigation } from '@react-navigation/native';

const SwipePicture = () => {
  const userService = new UserService();
  const whoService = new WhoService();
  const pictureService = new PictureService();
  const navigation = useNavigation<any>();
  const { user } = useUser();
  const [cards, setCards] = useState<Card[]>([]);
  const [friendList, setFriendList] = useState<Friend[]>([]);
  const [pictureList, setPictureList] = useState<Picture[]>([]);
  const [actualCard, setActualCard] = useState(0);

  const styles = StyleSheet.create({
    GlobalContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      backgroundColor: 'transparent',
      color: '#3D3D3D',
      width: '100%',
      gap: 20,
      marginTop: 40,
    },
    container: {
      flexDirection: 'row', // Utilisez une disposition en ligne pour organiser les éléments horizontalement
      justifyContent: 'center',
      backgroundColor: 'transparent',
      color: '#3D3D3D',
    },
    leftView: {
      // width: '20%', // 20% de la largeur pour la vue de gauche
      flex: 15,
      backgroundColor: 'transparent',
      zIndex: 1,
    },
    rightView: {
      // width: '20%', // 20% de la largeur pour la vue de droite
      flex: 15,
      backgroundColor: 'transparent',
      zIndex: 1,
    },
    topView: {
      width: '100%', // 100% de la largeur pour la vue de haut
      height: '15%', // 20% de la hauteur pour la vue de haut
      backgroundColor: 'transparent',
      zIndex: -1,
      alignItems: 'center', // Alignez le contenu au centre
      justifyContent: 'center', // Centrez le contenu verticalement
    },
    circle: {
      width: 60,
      height: 60,
      borderRadius: 30,
      backgroundColor: 'white',
      marginBottom: 10,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.8,
      shadowRadius: 2,
      elevation: 5,
    },
    friendNameText: {
      fontSize: 15,
      fontWeight: '300',
      color: '#FFFFFF',
      textShadowColor: 'rgba(0, 0, 0, 0.75)',
      textShadowOffset: { width: 0, height: 1 },
      textShadowRadius: 2,
    },
    swiperContainer: {
      width: '50%', // 50% de la largeur pour la balise Swiper
      // flex : 60,
      height: '100%', // Ajuster la hauteur selon vos besoins
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'transparent',
      maxWidth: '100%',
      zIndex: 99,
    },
    cardText: {
      fontSize: 20,
      fontWeight: 'bold',
      color: '#3D3D3D',
    },
    cardContainer: {
      backgroundColor: 'white',
      flexDirection: 'column',
      justifyContent: 'flex-start',
      height:275,
      alignItems: 'center',
      position: 'relative',
      width: '100%',
      borderRadius: 15,
      shadowColor: '#000',
      shadowOpacity: 0.31,
      shadowOffset: { width: 2, height: 4 },
      shadowRadius: 3,
    },
    swipeNativeStyle: {
      width: '100%',
      height: '100%',
      backgroundColor: 'transparent',
      top: 0,
      left: 0,
    },
    globalStyle: {
      width: '100%',
      height: '100%',
      backgroundColor: 'transparent',
      color: '#3D3D3D',
    },
    textContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    image: {
      width: 150,
      height: 150,
      marginBottom: 20,
    },
    imageStyle: {
      width: '100%',           // L'image occupe toute la largeur de la carte
      height: 200,             // Hauteur de l'image (tu peux ajuster cette valeur)
      resizeMode: 'cover',     // Couvre l'espace disponible en respectant l'aspect de l'image
      borderRadius: 10,        // Bord arrondi pour l'image (optionnel, tu peux ajuster)
      marginBottom: 10,        // Espace sous l'image (optionnel, tu peux ajuster)
    },
    commentSection: {
      marginTop: 10,
      textAlign: 'center',
    },
    textComment: {
      fontWeight: '400',
      fontSize: 13,
      fontFamily: 'Quicksand',
      textAlign: 'center',
      color:'black',

    },
 
    imagecontainer: {
      width: '100%',
      alignItems: 'center',
    },
  });

  useEffect(() => {
    getFriends();
    getImageUri();
  }, []);

  useEffect(() => {
    if (!friendList.length || !pictureList.length) return;
    getShuffledFriends();
  }, [friendList, pictureList]);

  const getFriends = async () => {
    if (!user) return;
    const friends = await userService.getUserFriends(user?.uid, [
      'id',
      'firstname',
    ]);
    setFriendList(friends);
  };
  console.log(friendList)

  const getImageUri = async () => {
    const pictures = await pictureService.getPictures();
    setPictureList(pictures);
  }


  const shuffle = (arr: any[]) => arr.sort(() => Math.random() - 0.5);

  const getShuffledFriends = async () => {
    if (!user || !pictureList.length || !friendList.length) return;
    
    const shuffledFriends = pictureList.map(picture => {
      const goodUser = friendList.find(friend => friend.id === picture.userId);
      if (!goodUser) return null; // Retourne null si aucun utilisateur trouvé
      const filteredFriends = friendList.filter(
        friend => friend.id !== goodUser.id,
      );
      return shuffle(filteredFriends).slice(0, 2).concat(goodUser);
    }).filter(Boolean); // Supprime les valeurs nulles ou undefined
    
    getCards(shuffledFriends);
  };

  const getCards = async (shuffledFriends: any[]) => {
    const formattedCards = pictureList
      .map((picture, i) => {
        const friend = friendList.find(
          friend => friend.id === picture.userId,
        ) as Friend;
  
        // Si `friend` ou `shuffledFriends[i]` n'est pas défini, passe à l'élément suivant
        if (!friend || !shuffledFriends[i]) return null;
  
        return {
          author: friend,
          shuffledFriends: shuffledFriends[i],
          comment: picture.comment || '',
          image: picture.image || { name: '', type: '', uri: '' },
        };
      })
      .filter(Boolean) as Card[]; // Cast pour indiquer que toutes les valeurs nulles sont supprimées
  
    if (!formattedCards.length) return; // Ne rien faire si aucune carte n'est formatée
    setCards(formattedCards);
  };
  
  return (
    <View style={styles.GlobalContainer}>
      <HeaderTitleFeed text={''} />
      {cards.length > 0 && (
        <View style={styles.globalStyle}>
          <View style={styles.topView}>
            {/* Contenu de la vue du haut */}
            <View style={styles.textContainer}>
              {/* add the circle */}
              <View style={styles.circle} />
              <Text style={styles.friendNameText}>Top Friend</Text>
              <Text style={styles.friendNameText}>
              {cards?.[actualCard]?.shuffledFriends?.[0]?.firstname || 'Ami manquant'}
              </Text>
            </View>
          </View>
          <View style={styles.container}>
            <View style={styles.leftView}>
              {/* Contenu de la vue de gauche */}
              <View style={styles.textContainer}>
                <View style={styles.circle} />
                <Text style={styles.friendNameText}>Left Friend</Text>
                <Text style={styles.friendNameText}>
                {cards?.[actualCard]?.shuffledFriends?.[1]?.firstname || 'Ami manquant'}
                </Text>
              </View>
            </View>

            <View style={styles.swiperContainer}>
              <Swiper
                cardStyle={styles.swipeNativeStyle}
                cards={cards}
                renderCard={card => {
                  if (!card) return null; // Évite de rendre une carte invalide
                  return (
                    <View style={styles.cardContainer}>
                     
                      <Image 
                        key={card.image.uri} 
                        source={{ uri: card.image.uri || '' }} 
                        fadeDuration={300}
                        style={styles.imageStyle}
                        resizeMode="cover"
                      />
    
                      
                      <View style={styles.commentSection}>
                      <Text style={styles.textComment}>{card.comment || 'Pas de commentaire'}</Text>
                          </View>
                      
                    </View>
                  );
                }}
                onSwipedLeft={cardIndex => {
                  if (!user) return;
                  whoService.ValidateFriend(
                    cards[cardIndex].shuffledFriends[1].id === cards[cardIndex].author.id,
                    user.uid,
                    "1",  // Utilisation de l'ID de la réponse
                  );
                }}
                onSwipedRight={cardIndex => {
                  if (!user) return;
                  whoService.ValidateFriend(
                    cards[cardIndex].shuffledFriends[2].id === cards[cardIndex].author.id,
                    user.uid,
                    "1",  // Utilisation de l'ID de la réponse
                  );
                }}
                onSwipedTop={cardIndex => {
                  if (!user) return;
                  whoService.ValidateFriend(
                    cards[cardIndex].shuffledFriends[0].id === cards[cardIndex].author.id,
                    user.uid,
                    "1",  // Utilisation de l'ID de la réponse
                  );
                }}
                onSwiped={(cardIndex): void => {
                  console.log(cardIndex);
                  console.log(cards[cardIndex]);
                  console.log('Nombre de carte swipe : ', cardIndex + 1);
                  if (cardIndex + 1 < cards.length) {
                    setActualCard(cardIndex + 1);
                  }
                }}
                onSwipedAll={() => {
                  console.log('Toutes les cartes sont swipe');
                  navigation.navigate('Feed');
                }}
                cardIndex={0}
                backgroundColor={'transparent'}
                stackSize={2}
                disableBottomSwipe={true}
              />

            </View>

            <View style={styles.rightView}>
              {/* Contenu de la vue de droite */}
              <View style={styles.textContainer}>
                <View style={styles.circle} />
                <Text style={styles.friendNameText}>Right Friend</Text>
                <Text style={styles.friendNameText}>
                {cards?.[actualCard]?.shuffledFriends?.[2]?.firstname || 'Ami manquant'}
                </Text>
              </View>
            </View>
          </View>
        </View>
      )}
    </View>
  );
};

export default SwipePicture;

interface Card {
  author: Friend;
  shuffledFriends: Friend[];
  comment: string
  image: {          // Structure de l'image
    name: string;
    type: string;
    uri: string;
  };
}
interface Friend {
  id: string;
  firstname: string;
}
interface Picture {
  comment: string;
  userId: string;
  image: {
    name: string;
    type: string;
    uri: string;
  };
}
interface Answer {
  id: string;
  userId: string;
  answer: string;
  comment: string
}
