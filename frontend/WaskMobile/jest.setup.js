/* global jest */
/* global jest */
import React from 'react';

jest.mock('react-native', () => {
  const rn = jest.requireActual('react-native');
  rn.Platform = {OS: 'android', Version: 26, select: jest.fn()};
  rn.Keyboard = {
    addListener: jest.fn(),
    removeListener: jest.fn(),
    dismiss: jest.fn(),
  };
  rn.BackHandler = {
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
  };
  return rn;
});

jest.mock('@react-navigation/bottom-tabs', () => {
  const originalModule = jest.requireActual('@react-navigation/bottom-tabs');
  return {
    ...originalModule,
    BottomTabBar: jest
      .fn()
      .mockImplementation(props => <div>Mock BottomTabBar</div>),
  };
});
jest.mock('@react-navigation/native', () => {
  const originalModule = jest.requireActual('@react-navigation/native');
  return {
    ...originalModule,
    NavigationContainer: jest
      .fn()
      .mockImplementation(props => <div>Mock NavigationContainer</div>),
  };
});
