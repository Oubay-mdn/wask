import * as React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {
  DefaultTheme as NavigationDefaultTheme,
  NavigationContainer,
} from '@react-navigation/native';
import {AppContext} from './config/context';
import {languages} from './config/language';
import Login from './src/components/LoginNav';
import Logged from './src/components/LoggedNav';
import {UserProvider, useUser} from './config/UserContext'; // Import the UserProvider and useUser hook
import {SafeAreaView, StatusBar, StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import './src/i18n';

const App: React.FC = () => {
  const [lang, setLang] = React.useState('fr');
  const [langContent] = React.useState(languages.fr);
  const {user} = useUser(); // Use the useUser hook to access user information

  const statusBarHeight = StatusBar.currentHeight;
  const styles = StyleSheet.create({
    LinearGradientStyle: {flex: 1},
  });

  type RootStackParamList = {
    Logged: undefined;
    Login: undefined;
  };

  const Stack = createNativeStackNavigator<RootStackParamList>();
  const CustomTheme = {
    ...NavigationDefaultTheme,
    colors: {
      ...NavigationDefaultTheme.colors,
      background: 'transparent',
    },
  };

  return (
    <LinearGradient
      colors={['rgba(0, 49, 175, 0.66)', '#99009C']}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 1}}
      style={styles.LinearGradientStyle}>
      {/*<StatusBar barStyle="light-content" translucent backgroundColor={"#5B7DD8"} />*/}
      <SafeAreaView style={{flex: 1, paddingTop: statusBarHeight}}>
        <AppContext.Provider value={{lang, setLang}}>
          <NavigationContainer theme={CustomTheme}>
            <Stack.Navigator
              screenOptions={{
                presentation: 'transparentModal',
                headerTransparent: true,
                headerShown: false, // This hides the header
              }}>
              {user ? (
                <Stack.Screen
                  name={'Logged'}
                  component={Logged}
                  options={{headerShown: false}}
                />
              ) : (
                <Stack.Screen
                  name={'Login'}
                  component={Login}
                  options={{headerShown: false}}
                />
              )}
            </Stack.Navigator>
          </NavigationContainer>
        </AppContext.Provider>
      </SafeAreaView>
    </LinearGradient>
  );
};

const AppWrapper: React.FC = () => {
  return (
    <UserProvider>
      <App />
    </UserProvider>
  );
};

export default AppWrapper;
