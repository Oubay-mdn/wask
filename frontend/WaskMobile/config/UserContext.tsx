import React, {createContext, useContext, useState, ReactNode} from 'react';
import auth, {FirebaseAuthTypes} from '@react-native-firebase/auth';

interface UserContextProps {
  user: FirebaseAuthTypes.User | null;
  updateUser: React.Dispatch<
    React.SetStateAction<FirebaseAuthTypes.User | null>
  >;
}

const UserContext = createContext<UserContextProps | undefined>(undefined);

interface UserProviderProps {
  children: ReactNode;
}

export const UserProvider: React.FC<UserProviderProps> = ({children}) => {
  const [user, setUser] = useState<FirebaseAuthTypes.User | null>(null);

  const updateUser = (
    userData: React.SetStateAction<FirebaseAuthTypes.User | null>,
  ) => {
    setUser(userData);
  };

  // Listen to authentication state changes
  auth().onAuthStateChanged(newUser => {
    setUser(newUser);
  });

  const contextValue: UserContextProps = {
    user,
    updateUser,
  };

  return (
    <UserContext.Provider value={contextValue}>{children}</UserContext.Provider>
  );
};

export const useUser = () => {
  const context = useContext(UserContext);
  if (!context) {
    throw new Error('useUser must be used within a UserProvider');
  }
  return context;
};
