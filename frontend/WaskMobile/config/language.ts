export const languages = {
  fr: {
    displayname: 'Français',
    navigation: {
      register: 'Inscription',
      login: 'Connexion',
      profile: 'Profil',
      feed: 'Accueil',
      game: 'Jeux',
      social: 'Social',
    },
    changinglang: 'Changer de langue',
    firstName: 'Prénom',
    lastName: 'Nom de famille',
    email: 'Courriel',
    loginName: "Nom d'utilisateur",
    password: 'Mot de passe',
    confirmPass: 'Confirmer le mot de passe',
    register: "S'inscrire",
    login: 'Se connecter',
    passwordForget: 'Mot de passe oublié',
    reinit:
      'Veuillez réinitialiser votre mot de passe via le lien envoyé par e-mail.',
    who: 'Connaissez vous vraiment vos amis?',
  },
  en: {
    displayname: 'English',
    navigation: {
      register: 'Register',
      login: 'Login',
      profile: 'Profile',
      feed: 'Feed',
      game: 'Game',
      social: 'Social',
    },
    changinglang: 'Changing language',
    firstName: 'First name',
    lastName: 'Last name',
    email: 'Email',
    loginName: 'Login name',
    password: 'Password',
    confirmPass: 'Confirm password',
    register: 'Register',
    login: 'Login',
    passwordForget: 'Forgot password',
    who: 'Do you really know your friends?',
    reinit: 'Please reset your password via the link sent by email.',
  },
};
