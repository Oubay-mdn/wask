module.exports = {
  preset: 'react-native',
  setupFiles: ['<rootDir>/jest.setup.js'],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|webp|svg)$': '<rootDir>/fileMock.js',
  },
  transformIgnorePatterns: [
    'node_modules/(?!(react-native|react-native-linear-gradient|@react-native/js-polyfills)/)',
  ],
};
