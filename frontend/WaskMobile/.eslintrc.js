module.exports = {
  root: true,
  extends: '@react-native',
  parser: '@babel/eslint-parser',
  plugins: ['@babel'],
  parserOptions: {
    requireConfigFile: false,
  },
};
