import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto, UpdateUserDto } from './dto';
import { User } from 'src/schemas/user.schema';

@Controller('/users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  createUser(@Body() createUserDto: CreateUserDto): Promise<User> {
    return this.userService.create(createUserDto);
  }

  @Get()
  getAllUsers(): Promise<User[]> {
    return this.userService.findAll();
  }

  @Get('/:id')
  getOneUser(@Param('id') id): Promise<User> {
    return this.userService.findOne(id);
  }

  @Delete('/:id')
  deleteUser(@Param('id') id) {
    return this.userService.deleteUser(id);
  }

  @Patch('/:id')
  updateUser(
    @Param('id') id,
    @Body() updateUserDto: UpdateUserDto,
  ): Promise<User> {
    return this.userService.update(id, updateUserDto);
  }
}
